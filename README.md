# My KDE

![alt text](img/1.png "Main")

## Theme
- **Theme**: [Materia Dark KDE](https://github.com/PapirusDevelopmentTeam/materia-kde)
- **Icons**: [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
- **Color Scheme**: [Blue](configs/blue.colorscheme)
- **Terminal Font**: [Fantasque Sans](https://github.com/belluzj/fantasque-sans/)

## Addons
- [**Application Title**](https://store.kde.org/p/1199712/)
- **Global Menu Applet**: preinstalled.

## Configs
- [Neofetch](configs/config.conf)